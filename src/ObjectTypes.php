<?php declare(strict_types=1);

namespace JLanger\Iobroker;

use MyCLabs\Enum\Enum;

/**
 * Class DBReturnTypes
 * @extends Enum<int>
 * @method static self STRING()
 * @method static self INT()
 * @method static self FLOAT()
 * @method static self BOOL()
 * @method static self DEFAULT()
 */
class ObjectTypes extends Enum
{
    public const DEFAULT = self::STRING;
    public const STRING  = 0;
    public const INT     = 1;
    public const FLOAT   = 2;
    public const BOOL    = 3;

    /**
     * ObjectTypes constructor.
     *
     * @param int $value
     */
    public function __construct(int $value = self::DEFAULT)
    {
        parent::__construct($value);
    }
}
