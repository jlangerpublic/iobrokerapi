<?php declare(strict_types=1);

namespace JLanger\Iobroker;

class IobConfig
{
    private string $hostname;
    private int    $port;
    
    /** @var IobObject[] */
    private array  $objects;

    /**
     * IobConfig constructor.
     * @deprecated - will be removed in next major version.
     * @param string $pathToFile
     *
     * @throws \Exception
     */
    public function __construct(string $pathToFile = '')
    {
        if ($pathToFile !== '') {
            $this->readFromFile($pathToFile);
        }
    }

    /**
     * @param string $pathToFile
     *
     * @return $this
     * @throws \Exception
     */
    public function readFromFile(string $pathToFile): self
    {
        $file = \file_get_contents($pathToFile);
        if ($file === false) {
            throw new \Exception('File could not be read.');
        }

        $this->readFromJson($file);

        return $this;
    }

    /**
     * read config from json string
     * @param string $json
     * @return $this
     *
     * @throws \JsonException
     */
    public function readFromJson(string $json): self
    {
        $json = \json_decode($json, true, 512, \JSON_THROW_ON_ERROR);

        $this->hostname = $json['hostname'];
        $this->port     = (int)$json['port'];

        $objects = $json['objects'];

        foreach ($objects as $name => $object) {
            $iobObject = new IobObject();
            $iobObject->setName($name);
            $iobObject->setDatapoint($object['datapoint']);

            $type = match ($object['type']) {
                'int'    => ObjectTypes::INT(),
                'float'  => ObjectTypes::FLOAT(),
                'bool'   => ObjectTypes::BOOL(),
                'string' => ObjectTypes::STRING(),
                default  => ObjectTypes::DEFAULT()
            };
            $iobObject->setType($type);

            $this->objects[$iobObject->getName()] = $iobObject;
        }

        return $this;
    }
    
    public function getHostname(): string
    {
        return $this->hostname;
    }
    
    public function getPort(): int
    {
        return $this->port;
    }

    /**
     * @return IobObject[]
     */
    public function getObjects(): array
    {
        return $this->objects;
    }
    
    public function getUrl(): string
    {
        return $this->hostname . ':' . $this->port . '/';
    }
}
