<?php declare(strict_types=1);

namespace JLanger\Iobroker;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class Iobroker
{
    private IobConfig $config;

    public function __construct(IobConfig $config)
    {
        $this->config = $config;
    }

    /**
     * @param IobObject $object
     *
     * @return bool|float|int|string
     * @throws GuzzleException
     */
    public function getValue(IobObject $object) :bool|float|int|string
    {
        $call = $this->callServer($object, 'getPlainValue');

        return $this->matchType($object, $call);
    }

    /**
     * @param IobObject $object
     * @param mixed     $value
     *
     * @throws GuzzleException
     */
    public function setValue(IobObject $object, mixed $value): void
    {
        $this->callServer($object, 'set', ['value=' . $value]);
    }

    /**
     * @param IobObject $object
     *
     * @throws GuzzleException
     */
    public function toggle(IobObject $object): void
    {
        if ($object->getType()->getValue() !== ObjectTypes::BOOL) {
            throw new \InvalidArgumentException('You can only trigger bools.');
        }

        $this->callServer($object, 'toggle');
    }

    /**
     * @param IobObject $object
     * @param string    $action
     * @param string[]  $params
     *
     * @return string
     * @throws GuzzleException
     */
    private function callServer(IobObject $object, string $action, array $params = []): string
    {
        $client = new Client([
            'base_uri' => $this->config->getUrl()
         ]);
        
        $url = $action . '/' . $object->getDatapoint();
        
        if (\count($params) > 0) {
            $url .= '?' . \implode('&', $params);
        }

        $response = $client->get($url);

        return (string)$response->getBody();
    }

    /**
     * @param IobObject $object
     * @param mixed     $value
     *
     * @return bool|float|int|string
     */
    private function matchType(IobObject $object, mixed $value) :bool|float|int|string
    {
        switch ($object->getType()) {
            case ObjectTypes::BOOL():
                return $value === 1 || $value === 'true' || $value === '1';
            case ObjectTypes::FLOAT():
                return (float)$value;
            case ObjectTypes::INT():
                return (int)$value;
            case ObjectTypes::STRING():
            case ObjectTypes::DEFAULT():
            default:
                return (string)$value;
        }
    }
}
