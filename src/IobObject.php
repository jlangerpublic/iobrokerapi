<?php declare(strict_types=1);

namespace JLanger\Iobroker;

class IobObject
{
    
    
    private string      $name;
    private string      $datapoint;
    private ObjectTypes $type;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getDatapoint(): string
    {
        return $this->datapoint;
    }

    /**
     * @param string $datapoint
     *
     * @return self
     */
    public function setDatapoint(string $datapoint): self
    {
        $this->datapoint = $datapoint;

        return $this;
    }

    /**
     * @return ObjectTypes
     */
    public function getType(): ObjectTypes
    {
        return $this->type;
    }

    /**
     * @param ObjectTypes $type
     *
     * @return self
     */
    public function setType(ObjectTypes $type): self
    {
        $this->type = $type;
        
        return $this;
    }
}
